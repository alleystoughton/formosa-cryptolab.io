---
name: Gilles Barthe
institution: Max Planck Institute for Security and Privacy & IMDEA Software Institute
website: https://gbarthe.github.io/

projects: EasyCrypt, Jasmin
---
