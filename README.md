# formosa-crypto.org

This is the source repo for the [Formosa Crypto
website](https://formosa-crypto.gitlab.io). Contributions by project members
are actively encouraged. In particular, please contribute by:
- adding yourself to the `_people` collection, and keeping the information up to date;
- adding any Formosa-relevant funding to the `_funding` collection, and keeping
  the information up to date.

The management of project-related information will likely be less distributed.

Ideally, the workflow for contribution will be that you fork the repo, add your
information, create a merge request, and assign your institution's lead as a
reviewer. If you are the first person in your institution to join Formosa, you
will have received other instructions.

If you'd like to see more information readily available on the Formosa Crypto
website itself, please open an issue and start a discussion on Zulip. For
information related to a specific Formosa project, please engage directly with
that project's team.

# Contributing content

## Adding a person

To add a person, pick a short name for them—it doesn't really matter what that
is, but please make it so they recognise it to avoid duplicates—and create a
file `_people/shortname.markdown` with the following front-matter. (See
[fdupress.markdown](https://gitlab.com/formosa-crypto/formosa-crypto.gitlab.io/-/blob/master/_people/fdupress.markdown).)

```markdown
---
name: <Full name>
institution: <Institution(s)>
website: <An external URL if it exists>

projects: <A comma-separated list of Formosa projects>
---
```

Any content you add below the front-matter will be displayed on the [people
page](https://formosa-crypto.gitlab.io/people), so please keep it short. The
people page is (or should be) sorted by name—so alphabetically on first name
for typical Western names.

## Adding funding

To add a funding source, pick a short name for it—it doesn't really matter what
that it, but please make it so all involved recognise it to avoid
duplicates—and create a file `_funding/shortname.markdown` with the following
front-matter. (See
[sample.markdown](https://gitlab.com/formosa-crypto/formosa-crypto.gitlab.io/-/blob/master/_funding/sample.markdown).)

```markdown
---
name: <Project/Grant Name>
funder: <Funder Name>
programme: <Funding Programme name>

number: <Grant number/identifier>

date: <Start date in ISO-8601 format>
end: <End date in ISO-8601 format>
---
```

As for people, content you add will be rendered from Markdown and displayed in
the list, so please keep it short. Ideally, simply use it for a short summary
of the funded project's goals and a link to the project website if relevant.
The people page is (or should be) sorted by start date.
